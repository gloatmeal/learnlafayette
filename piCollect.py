import pygatt
import time
import datetime
from binascii import hexlify

def convertToFahrenheitCelsius(data):
    data = hexlify(data)
    data = str(data)[2:len(data)+2]
    data = data[0:2] + " " + data[2:4] + " " + data[4:6] + " " + data[6:8]
    raw_temp_bytes = data.split() 
    raw_ambient_temp = int( '0x'+ raw_temp_bytes[3]+ raw_temp_bytes[2], 16) 
    ambient_temp_int = raw_ambient_temp >> 2 & 0x3FFF 
    ambient_temp_celsius = float(ambient_temp_int) * 0.03125
    ambient_temp_fahrenheit = (ambient_temp_celsius * 1.8) + 32
    return ambient_temp_fahrenheit

def convertToRH(data):
    data = hexlify(data)
    data = str(data)[2:len(data)+2]
    data = data[0:2] + " " + data[2:4] + " " + data[4:6] + " " + data[6:8]
    data = data.split()
    raw_humidity = int( '0x'+ data[3]+ data[2], 16)
    raw_humidity &= ~0x0003
    RH = (float(raw_humidity) / 65536)*100
    return RH

def convertTohPa(data):
    data = hexlify(data)
    data = str(data)[2:len(data)+2]
    data = int('0x' + data[6:11], 16)
    data = data/100.0
    return data

def convertToLux(data):
    data = hexlify(data)
    data = str(data)[2:len(data)+2]
    data = int('0x' + data, 16)
    m = data & 0x0FFF
    e = (data & 0xF000) >> 12
    if (e == 0):
        e = 1
    else:
        e = 2 << (e - 1)
    return m * (0.01 * e)
##    magnitude = pow(2.0,e)
##    output = (m * magnitude)
##    lux = output / 100.0
##    return lux

def collectTemperature(device):
    try:
        device.char_write_handle(0x0027, bytearray([1])) #turn on temperature sensor
        time.sleep(2)
        data = device.char_read_handle("0024")
        print("Time: " + str(datetime.datetime.now()))
        print("Raw Temp. Data: %s " % hexlify(data))
        fahrenheit = convertToFahrenheitCelsius(data)
        celsius = (fahrenheit - 32)*(5/9)
        fahrenheit = str(fahrenheit) + "F"
        print("Fahrenheit Value: " + fahrenheit)
        celsius = str(celsius) + "C"
        print("Celsius Value: " + celsius)
    except:
        print("Connection failed.")
    finally:
        device.char_write_handle(0x0027, bytearray([0])) #turn off temperature sensor
        
def collectPressure(device):
##    try:
        device.char_write_handle(0x0037, bytearray([1]))
        time.sleep(60)
        print("Time: " + str(datetime.datetime.now()))
        data = device.char_read_handle("0034")
        hPa = convertTohPa(data)
        print("Raw Pressure Data: %s " % hexlify(data))
        print("Pressure: " + str(hPa) + "hPa")
##    except:
##        print("Connection failed.")
##    finally:
##        device.char_write_handle(0x0037, bytearray([0])) #turn off temperature sensor
        
def collectHumidity(device):
    try:
        device.char_write_handle(0x002f, bytearray([1])) #turn on humidity sensor
        time.sleep(2)
        data = device.char_read_handle("002c")
        print("Raw Humidity Data: %s" % hexlify(data))
        humidity = str(convertToRH(data)) + "%RH"
        print("Relative Humidity: " + humidity)
    except:
        print("Connection failed.")
    finally:
        device.char_write_handle(0x002f, bytearray([0])) #turn off humidity sensor
        
def collectLight(device):
##    try:
        device.char_write_handle(0x0047, bytearray([1])) #turn on humidity sensor
        time.sleep(2)
        data = device.char_read_handle("0044")
        print("Raw Light Data: %s" % hexlify(data))
        lux = str(convertToLux(data))
        print("Lux (Intensity): " + lux)
##    except:
##        print("Connection failed.")
##    finally:
##        device.char_write_handle(0x0047, bytearray([0])) #turn off humidity sensor

def backgroundCollect(bleAddr, intervalSecs):
##    try:
        adapter = pygatt.GATTToolBackend()
        adapter.start()
        device = adapter.connect(bleAddr)
        print("Connected to " + bleAddr)
        while (True):
            #collectTemperature(device)
            collectPressure(device)
            time.sleep(intervalSecs)
##    except:
##        print("Connection failed.")   
##        adapter.stop()

minutesToWait = 0.25
backgroundCollect('24:71:89:BF:23:02', 60*minutesToWait)

def collectMeta():
        adapter = pygatt.GATTToolBackend()
        adapter.start()
        device = adapter.connect('24:71:89:BF:23:02')

        manufact = str(device.char_read_handle("0017"))
        manufact = "Manufacturer: " + manufact[12:len(manufact)-2]
        
        name = str(device.char_read_handle("0003"))
        name = "\nDevice Name: " + name[12:len(name)-2]
        
        model = str(device.char_read_handle("000d"))
        model = "\nModel Number: " + model[12:len(model)-2]
        
        systemID = str(hexlify(device.char_read_handle("000b")))
        systemID = "\nSystem ID: " + systemID[12:len(systemID)-2]
        print(manufact + name + model + systemID)
        
#collectMeta()
