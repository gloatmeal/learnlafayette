import json
import time
#hello
    
class Observation:
          
    def __init__(self, pTime, rTime, result, x):
        self.observeJSON =    {
        "@iot.id": x,
        
        "@iot.selfLink":
        "http://sensorthing.org/v1.0/Observations(" + str(x) + ")",

        "FeatureOfInterest@iot.navigationLink":
        "Observations(" + str(x) + ")/Datastream",

        "phenomenonTime": pTime,

        "resultTime": rTime,

        "result": result
        }

    def returnJSON(self):
        return self.observeJSON
    def returnResult(self):
        return self.observeJSON.get("result")
    


def measureTemp(measure, obsID):
    time1 = time.strftime("%H:%M:%S", time.gmtime())
    
    raw_temp_data = measure
    raw_temp_bytes = raw_temp_data.split() 
    raw_ambient_temp = int( '0x'+ raw_temp_bytes[3]+ raw_temp_bytes[2], 16) 
    ambient_temp_int = raw_ambient_temp >> 2 & 0x3FFF 
    ambient_temp_celsius = float(ambient_temp_int) * 0.03125
    ambient_temp_fahrenheit = (ambient_temp_celsius * 1.8) + 32 
    result = str(ambient_temp_fahrenheit)+" F"
    
    time2 = time.strftime("%H:%M:%S", time.gmtime())
    myOb = Observation(time1, time2, result, obsID)
    print(myOb.returnJSON())
    print()
    print("Result: " + myOb.returnResult())
    print()
    
def main():
    x = 1
    measureTemp("50 39 29 40", x)
    x += 1
    measureTemp("49 49 38 21", x)
    
main()
